/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9275334936447956, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.8651423877327492, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9999670064997196, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [0.8940955951265229, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.7504095004095004, 500, 1500, "getNotifications"], "isController": false}, {"data": [0.0, 500, 1500, "getHomefeed"], "isController": false}, {"data": [0.8106317934782609, 500, 1500, "me"], "isController": false}, {"data": [0.9567997504678727, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.8123947457056248, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.9024977085242897, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.018156424581005588, 500, 1500, "getChildCheckInCheckOut"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 58220, 0, 0.0, 256.7126073514296, 10, 13615, 94.0, 588.9000000000015, 971.0, 1847.0, 192.49144500834836, 675.9957318030782, 240.9481013962821], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getCountCheckInOutChildren", 3652, 0, 0.0, 409.3326944140209, 78, 1636, 311.0, 865.4000000000005, 1069.0, 1283.699999999998, 12.16733078348015, 7.6877568524527895, 15.054695412763035], "isController": false}, {"data": ["getLatestMobileVersion", 30309, 0, 0.0, 49.01511102312828, 10, 1080, 27.0, 103.0, 171.0, 249.9900000000016, 101.04684114019004, 67.59481072366228, 74.50230962972996], "isController": false}, {"data": ["findAllConfigByCategory", 4268, 0, 0.0, 350.23336457357004, 33, 1691, 248.0, 751.0999999999999, 960.5499999999997, 1230.1700000000028, 14.225481209899174, 16.087018790100828, 18.47645508707608], "isController": false}, {"data": ["getNotifications", 2442, 0, 0.0, 613.3419328419333, 75, 1917, 470.5, 1277.0, 1391.0, 1614.1300000000015, 8.121618071099078, 68.93064712492392, 9.033713850568216], "isController": false}, {"data": ["getHomefeed", 144, 0, 0.0, 10455.354166666666, 5006, 13615, 10474.0, 11973.5, 12333.5, 13431.400000000005, 0.4761101798307825, 8.326813682216292, 2.3824107045438763], "isController": false}, {"data": ["me", 2944, 0, 0.0, 508.081861413044, 79, 1577, 378.0, 1136.0, 1250.0, 1404.5500000000002, 9.801799882138683, 12.889010363430696, 31.061367790566432], "isController": false}, {"data": ["getAllClassInfo", 6412, 0, 0.0, 232.9737991266379, 20, 1406, 181.0, 471.0, 623.6999999999989, 1007.8699999999999, 21.376896149358227, 12.358518086347726, 54.88267575845975], "isController": false}, {"data": ["findAllChildrenByParent", 2969, 0, 0.0, 503.6746379252268, 65, 1665, 366.0, 1146.0, 1257.0, 1447.3000000000002, 9.891984460688606, 11.1091622361249, 15.803990798522033], "isController": false}, {"data": ["findAllSchoolConfig", 4364, 0, 0.0, 342.4653987167732, 37, 1571, 261.0, 689.5, 892.75, 1179.4000000000015, 14.548121478814547, 317.27407115711566, 10.669569561122778], "isController": false}, {"data": ["getChildCheckInCheckOut", 716, 0, 0.0, 2077.692737430167, 1070, 3134, 2019.5, 2687.2000000000003, 2794.6, 3008.9600000000005, 2.3764400529720433, 158.38369560076472, 10.935337431254169], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 58220, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
